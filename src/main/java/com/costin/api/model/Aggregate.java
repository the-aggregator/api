package com.costin.api.model;

public class Aggregate {

    private final String minute;
    private final Long count;
    private final Long sum;
    private final Double average;

    public Aggregate(String minute, Long count, Long sum, Double average) {
        this.minute = minute;
        this.count = count;
        this.sum = sum;
        this.average = average;
    }

    public String getMinute() {
        return minute;
    }

    public Long getCount() {
        return count;
    }

    public Long getSum() {
        return sum;
    }

    public Double getAverage() {
        return average;
    }

    @Override
    public String toString() {
        return "Aggregate{" +
                "minute='" + minute + '\'' +
                ", count=" + count +
                ", sum=" + sum +
                ", average=" + average +
                '}';
    }
}
