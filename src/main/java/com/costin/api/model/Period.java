package com.costin.api.model;

public enum Period {
    LAST_15_MINUTES(15),
    LAST_30_MINUTES(30),
    LAST_45_MINUTES(45),
    LAST_HOUR(60),
    LAST_2_HOURS(120);

    private final int minutes;

    Period(int minutes) {
        this.minutes = minutes;
    }

    public int getMinutes() {
        return minutes;
    }
}
