package com.costin.api.controller;

public class ErrorBody {
    private String errorMessage;
    private Integer statusCode;

    public ErrorBody() {
    }

    public ErrorBody(String errorMessage, Integer statusCode) {
        this.errorMessage = errorMessage;
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "ErrorBody{" +
                "errorMessage='" + errorMessage + '\'' +
                ", statusCod='" + statusCode + '\'' +
                '}';
    }
}
