package com.costin.api.controller;

import com.costin.api.exception.IllegalRequestInputException;
import com.costin.api.exception.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<ErrorBody> handleException(ResourceNotFoundException ex, WebRequest request) {
        return new ResponseEntity<>(
                new ErrorBody(ex.getMessage(), HttpStatus.NOT_FOUND.value()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalRequestInputException.class)
    public final ResponseEntity<ErrorBody> handleException(
            IllegalRequestInputException ex, WebRequest request
    ) {
        return new ResponseEntity<>(
                new ErrorBody(ex.getMessage(), HttpStatus.PRECONDITION_FAILED.value()),
                HttpStatus.PRECONDITION_FAILED);
    }

}