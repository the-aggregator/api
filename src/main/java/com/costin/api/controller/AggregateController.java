package com.costin.api.controller;

import com.costin.api.exception.IllegalRequestInputException;
import com.costin.api.exception.ResourceNotFoundException;
import com.costin.api.model.Aggregate;
import com.costin.api.model.Period;
import com.costin.api.service.AggregateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AggregateController {

    private final AggregateService aggregateService;

    @Autowired
    public AggregateController(AggregateService aggregateService) {
        this.aggregateService = aggregateService;
    }

    @GetMapping(value = "/aggregates/{serverName}/{minute}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Aggregate getAggregates(
            @PathVariable String serverName,
            @PathVariable String minute
    ) {
        return aggregateService.get(serverName, minute).orElseThrow(ResourceNotFoundException::new);
    }

    @GetMapping(value = "/aggregates/{serverName}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Aggregate> getAggregates(
            @PathVariable String serverName,
            @RequestParam("period") Period period
    ) {
        if (period == null) {
            throw new IllegalRequestInputException();
        }
        return aggregateService.get(serverName, period);
    }

}
