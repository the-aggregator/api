package com.costin.api.repository;

import com.costin.api.model.Aggregate;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AggregateRepository {

    private final Session session;
    private final PreparedStatement selectStatement;

    @Autowired
    public AggregateRepository(Session session) {
        this.session = session;
        this.selectStatement = session.prepare(
                "SELECT minute, count, sum, average " +
                "FROM aggregate " +
                "WHERE server_name = :server_name " +
                "AND minute in :minutes");
    }

    public List<Aggregate> get(String serverName, List<String> minutes) {
        BoundStatement statement = selectStatement.bind(serverName, minutes);
        return session.execute(statement).all()
                .stream().map(this::createEntity)
                .collect(Collectors.toList());
    }

    private Aggregate createEntity(Row row) {
        return new Aggregate(
                row.getString("minute"),
                row.getLong("count"),
                row.getLong("sum"),
                row.getDouble("average")
        );
    }
}
