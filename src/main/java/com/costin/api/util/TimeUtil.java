package com.costin.api.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TimeUtil {

    private TimeUtil() { }

    public static String fromLocalDateTimeToMinuteOfDay(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern("dd-MM-yyyy'T'hh:mm"));
    }

    public static List<String> getMinutesOfDayInTheLastMinutes(int minutes) {
        return getMinutesOfDayInTheLastMinutes(getLocalDateTimeInUTC(), minutes);
    }

    public static List<String> getMinutesOfDayInTheLastMinutes(LocalDateTime localDateTime, int minutes) {
        return Stream.iterate(localDateTime, date -> date.minusMinutes(1))
                .limit(minutes)
                .map(TimeUtil::fromLocalDateTimeToMinuteOfDay)
                .collect(Collectors.toList());
    }

    public static LocalDateTime getLocalDateTimeInUTC() {
        return LocalDateTime.now(ZoneId.of("UTC"));
    }
}
