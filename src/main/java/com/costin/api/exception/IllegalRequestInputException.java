package com.costin.api.exception;

public class IllegalRequestInputException extends RuntimeException {
    public IllegalRequestInputException() {
        super("Illegal request input exception");
    }
}
