package com.costin.api.config;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CassandraConfig {

    @Value("${cassandra.keyspace}")
    private String keyspace;

    @Value("${cassandra.contact-point}")
    private String contactPoint;

    @Bean
    public Session cqlSession() {
        return Cluster.builder()
                .addContactPoint(contactPoint)
                .withoutMetrics()
                .build()
                .connect(keyspace);
    }
}
