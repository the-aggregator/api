package com.costin.api.service;

import com.costin.api.model.Aggregate;
import com.costin.api.model.Period;
import com.costin.api.repository.AggregateRepository;
import com.costin.api.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class AggregateService {

    private final AggregateRepository aggregateRepository;

    @Autowired
    public AggregateService(AggregateRepository aggregateRepository) {
        this.aggregateRepository = aggregateRepository;
    }

    public Optional<Aggregate> get(String serverName, String minute) {
        return aggregateRepository
                .get(serverName, Collections.singletonList(minute))
                .stream()
                .findFirst();
    }

    public List<Aggregate> get(String serverName, Period period) {
        var minutes = TimeUtil.getMinutesOfDayInTheLastMinutes(period.getMinutes());
        return aggregateRepository.get(serverName, minutes);
    }
}
