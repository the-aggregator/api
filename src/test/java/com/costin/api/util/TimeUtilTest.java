package com.costin.api.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

class TimeUtilTest {

    @Test
    void test() {
        String instantExpected = "2020-09-09T10:15:20Z";
        var clock = Clock.fixed(Instant.parse(instantExpected), ZoneId.of("UTC"));
        var minutes = TimeUtil.getMinutesOfDayInTheLastMinutes(LocalDateTime.now(clock), 6);
        var expected = List.of(
                "09-09-2020T10:15", "09-09-2020T10:14", "09-09-2020T10:13",
                "09-09-2020T10:12", "09-09-2020T10:11", "09-09-2020T10:10"
        );
        Assertions.assertEquals(expected, minutes);
    }
}